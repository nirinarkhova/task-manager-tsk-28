package ru.nirinarkhova.tm.api;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.nirinarkhova.tm.model.AbstractEntity;

import java.util.List;
import java.util.Optional;

public interface IRepository<E extends AbstractEntity> {

    @NotNull
    List<E> findAll();

    @NotNull
    E  add(@NotNull E entity);

    void addAll(@NotNull List<E> entity);

    @Nullable
    Optional<E> findById(@NotNull String id);

    void clear();

    @Nullable
    E removeById(@NotNull String id);

    void remove(@Nullable E entity);

}