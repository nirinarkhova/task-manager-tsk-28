package ru.nirinarkhova.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.nirinarkhova.tm.command.AbstractTaskCommand;
import ru.nirinarkhova.tm.exception.entity.TaskNotFoundException;
import ru.nirinarkhova.tm.model.Task;
import ru.nirinarkhova.tm.util.TerminalUtil;

import java.util.Optional;

public class TaskStartByIdCommand extends AbstractTaskCommand {

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String name() {
        return "task-start-by-id";
    }

    @NotNull
    @Override
    public String description() {
        return "change task status to In progress by task id.";
    }

    @Override
    public void execute() {
        @NotNull final String userId = serviceLocator.getAuthService().getUserId();
        System.out.println("[TASK START]");
        System.out.println("[ENTER TASK ID:]");
        @NotNull final String id = TerminalUtil.nextLine();
        @NotNull final Optional<Task> task = serviceLocator.getTaskService().startById(userId, id);
        Optional.ofNullable(task).orElseThrow(TaskNotFoundException::new);
    }

}

