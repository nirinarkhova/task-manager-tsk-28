package ru.nirinarkhova.tm.enumerated;

import org.jetbrains.annotations.NotNull;
import ru.nirinarkhova.tm.comparator.ComparatorByCreated;
import ru.nirinarkhova.tm.comparator.ComparatorByDateStart;
import ru.nirinarkhova.tm.comparator.ComparatorByName;
import ru.nirinarkhova.tm.comparator.ComparatorByStatus;

import java.util.Comparator;

public enum Sort {

    CREATED("Sort by created date", ComparatorByCreated.getInstance()),
    DATE_START("Sort by start date", ComparatorByDateStart.getInstance()),
    NAME("Sort by name", ComparatorByName.getInstance()),
    STATUS("Sort by status", ComparatorByStatus.getInstance());

    @NotNull
    private final String displayName;

    @NotNull
    private final Comparator comparator;

    Sort(@NotNull String displayName,@NotNull Comparator comparator) {
        this.displayName = displayName;
        this.comparator = comparator;
    }

    @NotNull
    public Comparator getComparator() {
        return comparator;
    }

    @NotNull
    public String getDisplayName() {
        return displayName;
    }

}
