package ru.nirinarkhova.tm.exception.system;

import ru.nirinarkhova.tm.exception.AbstractException;

public class NullObjectException extends AbstractException {

    public NullObjectException() {
        super("Error! Null Object...");
    }

}
