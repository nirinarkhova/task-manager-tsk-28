package ru.nirinarkhova.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.nirinarkhova.tm.api.IRepository;
import ru.nirinarkhova.tm.api.IService;
import ru.nirinarkhova.tm.exception.empty.EmptyIdException;
import ru.nirinarkhova.tm.exception.entity.ObjectNotFoundException;
import ru.nirinarkhova.tm.exception.system.NullObjectException;
import ru.nirinarkhova.tm.model.AbstractEntity;

import java.util.List;
import java.util.Optional;

public abstract class AbstractService<E extends AbstractEntity> implements IService<E> {

    private IRepository<E> repository;

    @NotNull
    public AbstractService(@NotNull IRepository<E> repository) {
        this.repository = repository;
    }

    @NotNull
    public List<E> findAll() {
        return repository.findAll();
    }

    @NotNull
    public E add(@Nullable final E entity) {
        if (!Optional.ofNullable(entity).isPresent()) throw new ObjectNotFoundException();
        return  repository.add(entity);
    }

    @Nullable
    public Optional<E> findById(@Nullable final String id) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        return repository.findById(id);
    }

    public void clear() {
        repository.clear();
    }

    @Nullable
    public E removeById(@Nullable final String id) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        return repository.removeById(id);
    }

    public void remove(@Nullable final E entity) {
        if (!Optional.ofNullable(entity).isPresent()) throw new ObjectNotFoundException();
        repository.remove(entity);
    }

    @Override
    public void addAll(@Nullable List<E> entity) {
        if (entity == null) throw new NullObjectException();
        repository.addAll(entity);
    }

}
